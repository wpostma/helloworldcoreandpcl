# Example of creating a multi-target project.json

This is a repository that shows how to create a multi-targeted (different .net framework versions) class library using a project.json.

Stack Overflow Reference:

http://stackoverflow.com/questions/38206209/create-a-single-source-project-with-full-net-pcl-and-dotnet-core-assemblies-bu/38207421?noredirect=1#comment63840041_38207421
The repaired version of this repo is here:

https://bitbucket.org/wpostma/helloworldcoreandpclv2