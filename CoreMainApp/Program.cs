﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// "WindowsAzure.Storage": "7.1.3-preview"

namespace CoreMainApp
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var message = HelloWorldPCL.HelloWorldPortableClass.GetMessage();

            Console.Out.WriteLine(message);
        }
    }
}

/*
 * {
  "version": "1.0.0-*",
  "buildOptions": {
    "emitEntryPoint": true
  },

  "dependencies": {
    "NETStandard.Library": "1.6.0"
  
  },
  "frameworks": {
    "netstandard1.6": {
      "imports": "dnxcore50",
      "dependencies": {
        "HelloWorldPCL": {
          "target": "project"
        },
        "System.Runtime": "4.1.0"
      }
    }
  }

}

 */

