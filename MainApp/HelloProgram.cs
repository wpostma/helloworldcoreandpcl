﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

// This is a .net 4.x console application that is intended to load
// a PCL and execute it.


namespace MainApp
{
    class HelloProgram
    {
        static void Main(string[] args)
        {
            var message = HelloWorldPCL.HelloWorldPortableClass.GetMessage();

            Console.Out.WriteLine(message);

        }
    }
}
